define([
    'jquery',
    'underscore',
    'Magento_Ui/js/form/form',
    'ko',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-service',
    'Magento_Checkout/js/action/select-shipping-method',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Checkout/js/checkout-data',
    'mage/translate',
], function (
    $,
    _,
    Component,
    ko,
    quote,
    shippingService,
    selectShippingMethodAction,
    stepNavigator,
    checkoutData,
    $t
) {
    'use strict';
    return Component.extend({
        defaults: {
            template: 'LearnM2_Checkout/shippingMethodStep',
            shippingMethodListTemplate: 'Magento_Checkout/shipping-address/shipping-method-list',
            shippingMethodItemTemplate: 'Magento_Checkout/shipping-address/shipping-method-item',
        },
        visible: ko.observable(!quote.isVirtual()),
        errorValidationMessage: ko.observable(false),

        /**
         * @return {exports}
         */
        initialize: function () {
            var self = this;
            this._super();

            if (!quote.isVirtual()) {
                stepNavigator.registerStep(
                    'opc-shipping_method',
                    null,
                    'Shipping Method',
                    this.visible,
                    _.bind(this.navigate, this),
                    15
                );
            }

            quote.shippingMethod.subscribe(function () {
                self.errorValidationMessage(false);
            });

            return this;
        },

        /**
         * Navigator change hash handler.
         *
         * @param {Object} step - navigation step
         */
        navigate: function (step) {
            step && step.isVisible(true);
        },

        /**
         * Shipping Method View
         */
        rates: shippingService.getShippingRates(),
        isLoading: shippingService.isLoading,
        isSelected: ko.computed(function () {
            return quote.shippingMethod() ?
                quote.shippingMethod()['carrier_code'] + '_' + quote.shippingMethod()['method_code'] :
                null;
        }),

        /**
         * @param {Object} shippingMethod
         * @return {Boolean}
         */
        selectShippingMethod: function (shippingMethod) {
            selectShippingMethodAction(shippingMethod);
            checkoutData.setSelectedShippingRate(shippingMethod['carrier_code'] + '_' + shippingMethod['method_code']);

            return true;
        },

        /**
         * @return {Boolean}
         */
        validateShippingMethod: function () {
            if (!quote.shippingMethod()) {
                this.errorValidationMessage(
                    $t('The shipping method is missing. Select the shipping method and try again.')
                );

                return false;
            }

            stepNavigator.next();
        },
    });
});
